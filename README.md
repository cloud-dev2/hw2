# Background
This repo contains bash scripts for creating ec2 instance, with all the needed configuratios.
Please note that we have added a script to delete your resources.
This will help avoid from manuall deletion and dependency searching between the resources. 

## Architecture explanation

![Alt Text](/arch.png)

Because using docker was "too much" here - We decided to create ami and push it - and use it for the launch template - we could not install all the dependencied on the instance because it does not have a public IP - it can only connect with the ALB.

We only implemented health check to the instance (we have load balancer - we don't need queue)
Our tests did no work though - The infrastructure is successfully created - but we think there is something related with the load balancer that causes the following error when trying to curl:
```
html>
<head><title>503 Service Temporarily Unavailable</title></head>
<body>
<center><h1>503 Service Temporarily Unavailable</h1></center>
</body>
</html>
```
Which we could not solve.
Things we should have might done:
1. ROUTE53 - BIG MAYBE - We think curl to the DNS name of the load balancer should work.
2. The MAIN complaint  - Somthing is worng with the helath checks.
    Recieving status 503 from the curl means that the load balancer recieved the http request, but did not forward the request. It seems to happen because there were no healthy instances.
    So - We think it is related to the health checks - they fail for unkown reason - and this is also why we see that new instances are created, although we set the desired capacity to be 2; They are created because the running instances considered "unhealthy".
    

We would love to know how could that be solved please.

## Run tests (curl)

In order to to run the tests - you will have to call directly to the load balancer.

Please run the `make test`.

You can modify infra/test.sh script to test 

## Requirements to run locally
In order to run the script from local, make sure you have:
```bash 
    aws-cli
    make
```

## AWS credentials
Your creds can be found under `~/.aws/` directory. This directory, with this file, is automatically created once you have downloaded the aws-cli.
Make sure to define `[default]` above the `aws-secret-access-key` and `aws-access-key-id`. See [this link](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-files.html) as an example for how to configure it.

**This script WORKS ONLY for region us-east-1.**

If you want it to work for other region, you need to change the `AMI_ID` variable's value in the `defaults.sh` file.

Alternatively, you can run from your terminal the command `aws configure`, and set your credentials there. 

## Execute the script
In order to set up the infra and deploy the app, you shall use the following `make` command:
```bash
    make up
```

## Once you are finished..
In order to delete the infra, you shall use  the following `make` command:
```bash
    make down
```
Please note the while running `make up`, the script writes variables to the `variables.sh` file. This file is necessary to be able to successfully complete the deletion process. Do not touch this file.
