source variables.sh


echo "Deleting ASG: $ASG_NAME"
aws autoscaling delete-auto-scaling-group --auto-scaling-group-name $ASG_NAME --force-delete

echo "Deleting s3 bucket: $BUCKET_NAME"
aws s3 rb s3://$BUCKET_NAME --force

# 2. Delete the listener
echo "Deleting listener: $LISTENER_ARN"
aws elbv2 delete-listener --listener-arn $LISTENER_ARN


# 2. Delete the target group
echo "Deleting target group: $TARGET_GROUP_ARN"
aws elbv2 delete-target-group --target-group-arn $TARGET_GROUP_ARN

# 1. Delete the load balancer
echo "Deleting load balancer: $LOAD_BALANCER_ARN"
aws elbv2 delete-load-balancer --load-balancer-arn $LOAD_BALANCER_ARN


echo "Deleting launch template: $LAUNCH_TEMPLATE_NAME"
aws ec2 delete-launch-template --launch-template-name $LAUNCH_TEMPLATE_NAME

# 6. Delete the security group
echo "Deleting security group: $SECURITY_GROUP_ID"
aws ec2 delete-security-group --group-id $SECURITY_GROUP_ID


# for subnet_id in "${SUBNET_IDS[@]}"; do
#     echo "Deleting subnet $subnet_id"
#     aws ec2 delete-subnet --subnet-id $subnet_id
# done




# Delete the vpc
echo "Deleting vpc: $VPC_ID"
aws ec2 delete-vpc --vpc-id $VPC_ID

# 4. Remove the variables file
echo "Removing the content of the variables file..."
> variables.sh

echo "Cleanup complete!"