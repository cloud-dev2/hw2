
source variables.sh
LOAD_BALANCER_IP=$(aws elbv2 describe-load-balancers \
  --load-balancer-arns $LOAD_BALANCER_ARN \
  --query 'LoadBalancers[0].DNSName' \
  --output text)

echo "Load Balancer IP: $LOAD_BALANCER_IP"

NUM_ITERATIONS=10

curl -X  PUT "http://$LOAD_BALANCER_IP:5000/enqueue?iterations=$NUM_ITERATIONS"