#!/bin/bash

source vpc.sh

SUBNET_NAMES=("sb1" "sb2")  # Names of your subnets (comma-separated list)
SUBNET_CIDRS=("10.0.1.0/24" "10.0.2.0/24")
AVAILABILITY_ZONES=("use1-az1" "use1-az2")

# Create subnets and retrieve their IDs
SUBNET_IDS=()
for ((i=0; i<${#SUBNET_NAMES[@]}; i++)); do
  SUBNET_ID=$(aws ec2 create-subnet \
    --vpc-id $VPC_ID \
    --availability-zone-id "${AVAILABILITY_ZONES[$i]}" \
    --cidr-block "${SUBNET_CIDRS[$i]}" --query 'Subnet.SubnetId' \
    --output text)
  
  # Add a name tag to the subnet
  aws ec2 create-tags \
    --resources $SUBNET_ID \
    --tags Key=Name,Value="${SUBNET_NAMES[$i]}"
  
  SUBNET_IDS+=("$SUBNET_ID")
done

echo "Subnet IDs: ${SUBNET_IDS[@]}"

printf "SUBNET_IDS=(%s)\n" "$(printf "\"%s\" " "${SUBNET_IDS[@]}")" >> "variables.sh"
