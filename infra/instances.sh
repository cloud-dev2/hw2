#!/bin/bash

source role.sh

echo "in instances.sh"

# Create load balancer
LOAD_BALANCER_ARN=$(aws elbv2 create-load-balancer \
  --name $LOAD_BALANCER_NAME \
  --subnets ${SUBNET_IDS[@]} \
  --security-groups $SECURITY_GROUP_ID \
  --query 'LoadBalancers[0].LoadBalancerArn' \
  --output text)

echo -e "LOAD_BALANCER_ARN=\"${LOAD_BALANCER_ARN}\"" >>"variables.sh"

# Create listener for the load balancer
LISTENER_ARN=$(aws elbv2 create-listener \
  --load-balancer-arn $LOAD_BALANCER_ARN \
  --protocol HTTP \
  --port 80 \
  --protocol HTTP \
  --port 5000 \
  --default-actions Type=forward,TargetGroupArn=$TARGET_GROUP_ARN \
  --query 'Listeners[0].ListenerArn' \
  --output text)

echo -e "LISTENER_ARN=\"${LISTENER_ARN}\"" >>"variables.sh"

echo "Creating Ubuntu 20.04 instance..."
INSTANCE_ID=$(aws ec2 run-instances \
  --image-id $AMI_ID \
  --instance-type $INSTANCE_TYPE \
  --key-name $KEY_NAME \
  --security-group-ids $SECURITY_GROUP_ID \
  --subnet-id "${SUBNET_IDS[0]}" \
  --associate-public-ip-address \
  --query 'Instances[*].[InstanceId]' \
  --output text)


echo "Waiting for instance creation..."
aws ec2 wait instance-running --instance-ids $INSTANCE_ID

PUBLIC_IP=$(aws ec2 describe-instances  --instance-ids $INSTANCE_ID | 
    jq -r '.Reservations[0].Instances[0].PublicIpAddress'
)

echo "New instance $INSTANCE_ID @ $PUBLIC_IP"


echo "deploying code to production"
scp -i $KEY_PEM -o "StrictHostKeyChecking=no" -o "ConnectionAttempts=60" $SCRIPT_PATH ubuntu@$PUBLIC_IP:/home/ubuntu/

echo "setup production environment"
ssh -i $KEY_PEM -o "StrictHostKeyChecking=no" -o "ConnectionAttempts=10" ubuntu@$PUBLIC_IP <<EOF
    sudo apt update
    sudo apt install python3-pip -y
    sudo pip3 install Flask
    # run app
    sudo python3 app.py &>/dev/null &
    exit
EOF

IMAGE_ID=$(aws ec2 create-image --instance-id $INSTANCE_ID --name $IMAGE_NAME --description "Image for backup purposes" --output text)


# Wait for the AMI to be available
aws ec2 wait image-available --image-ids $IMAGE_ID

echo "Image ID: $IMAGE_ID"

# Create launch template with IAM role and S3 access
LAUNCH_TEMPLATE_OUTPUT=$(aws ec2 create-launch-template \
  --launch-template-name $LAUNCH_TEMPLATE_NAME \
  --version-description "Version 1" \
  --launch-template-data '{
    "ImageId": "'$IMAGE_ID'",
    "IamInstanceProfile": {
      "Name": "'$INSTANCE_PROFILE_NAME'"
    },
    "InstanceType": "'$INSTANCE_TYPE'",
    "BlockDeviceMappings": [
      {
        "DeviceName": "/dev/xvda",
        "Ebs": {
          "VolumeSize": 30,
          "VolumeType": "gp2"
        }
      }
    ],
    "TagSpecifications": [
      {
        "ResourceType": "instance",
        "Tags": [
          {
            "Key": "Name",
            "Value": "MyInstance"
          }
        ]
      }
    ],
    "SecurityGroupIds": [
      "'$SECURITY_GROUP_ID'"
    ]
    }')

# Extract the Launch Template ID from the output
LAUNCH_TEMPLATE_ID=$(echo "$LAUNCH_TEMPLATE_OUTPUT" | jq -r '.LaunchTemplate.LaunchTemplateId')

# Check if the Launch Template ID is not empty
if [ -n "$LAUNCH_TEMPLATE_ID" ]; then
  echo "Launch Template ID: $LAUNCH_TEMPLATE_ID"
else
  echo "Failed to create Launch Template"
fi

aws autoscaling create-auto-scaling-group \
  --auto-scaling-group-name $ASG_NAME \
  --launch-template "LaunchTemplateId=$LAUNCH_TEMPLATE_ID,Version=1" \
  --min-size 1 \
  --max-size 5 \
  --desired-capacity 2 \
  --vpc-zone-identifier "$(
    IFS=','
    echo "${SUBNET_IDS[*]}"
  )" \
  --health-check-type "ELB" \
  --health-check-grace-period 300 \
  --termination-policies "Default" \
  --target-group-arns $TARGET_GROUP_ARN

# Attach the load balancer to the Auto Scaling Group
aws autoscaling attach-load-balancer-target-groups \
  --auto-scaling-group-name $ASG_NAME \
  --target-group-arns $TARGET_GROUP_ARN

aws ec2 terminate-instances --instance-ids $INSTANCE_ID --output text

