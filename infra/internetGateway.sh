#!/bin/bash

source subnets.sh

INTERNET_GATEWAY_ID=$(aws ec2 create-internet-gateway --query 'InternetGateway.InternetGatewayId' --output text)


aws ec2 create-tags \
  --resources $INTERNET_GATEWAY_ID \
  --tags Key=Name,Value="$INTERNET_GATEWAY_NAME"

echo Internet Gateway ID: $INTERNET_GATEWAY_ID

aws ec2 attach-internet-gateway --internet-gateway-id $INTERNET_GATEWAY_ID --vpc-id $VPC_ID

ROUTE_TABLE_ID=$(aws ec2 create-route-table --vpc-id $VPC_ID --query 'RouteTable.RouteTableId' --output text)

aws ec2 create-route --route-table-id $ROUTE_TABLE_ID --destination-cidr-block 0.0.0.0/0 --gateway-id $INTERNET_GATEWAY_ID


for ((i=0; i<${#SUBNET_IDS[@]}; i++)); do
  aws ec2 associate-route-table --subnet-id "${SUBNET_IDS[$i]}" --route-table-id $ROUTE_TABLE_ID
done


