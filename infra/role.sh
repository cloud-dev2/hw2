source targetGroup.sh

# Create the IAM role for EC2 instances
aws iam create-role \
  --role-name $IAM_ROLE_NAME \
  --assume-role-policy-document '{
    "Version": "2012-10-17",
    "Statement": [
      {
        "Sid": "",
        "Effect": "Allow",
        "Principal": {
          "Service": "ec2.amazonaws.com"
        },
        "Action": "sts:AssumeRole"
      }
    ]
  }' \
  --output text

# Attach s3 access policy to the IAM role of the EC2
aws iam attach-role-policy \
  --role-name $IAM_ROLE_NAME \
  --policy-arn "arn:aws:iam::aws:policy/AmazonS3FullAccess"

# Create the IAM instance profile
aws iam create-instance-profile \
  --instance-profile-name $INSTANCE_PROFILE_NAME \
  --output text

# Add the IAM role to the instance profile
aws iam add-role-to-instance-profile \
  --instance-profile-name $INSTANCE_PROFILE_NAME \
  --role-name $IAM_ROLE_NAME
  
echo -e "IAM_ROLE_NAME=\"${IAM_ROLE_NAME}\"" >> "variables.sh"

