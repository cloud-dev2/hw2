AWS_ACCOUNT_ID=$(aws sts get-caller-identity --query "Account" --output text)

RANDOM_SIX=""
for ((i=1; i<=6; i++)); do
  RANDOM_DIGIT=$(( RANDOM % 10 ))  # Generate a random digit (0-9)
  RANDOM_SIX="${RANDOM_SIX}${RANDOM_DIGIT}"
done


AMI_ID="ami-0261755bbcb8c4a84"
CURRENT_DATE=$(date +"%Y-%m-%d-%H:%M:%S")
MIN_SIZE="1"
MAX_SIZE="5"
DESIRED_CAPACITY="2"
KEY_NAME="key-cloud-course-$CURRENT_DATE"
TARGET_GROUP_NAME="hw2-tg-$RANDOM_SIX"
KEY_PEM="$KEY_NAME.pem"
SECURITY_GROUP_NAME="hw2-sg-$CURRENT_DATE"
LOAD_BALANCER_NAME="hw2-alb-$RANDOM_SIX"
INTERNET_GATEWAY_NAME="igw-cc-$RANDOM_SIX"
PROFILE_NAME="instanceProfile-$RANDOM_SIX"
ASG_NAME="hw2-$RANDOM_SIX"
LAUNCH_TEMPLATE_NAME="hw2-lt-$RANDOM_SIX"
IAM_ROLE_NAME="hw2-iam-$RANDOM_SIX"
INSTANCE_PROFILE_NAME="hw2-profile-$RANDOM_SIX"
INSTANCE_TYPE="t2.micro"
BUCKET_NAME="hw2-bucket-$RANDOM_SIX"
SCRIPT_PATH="../app/app.py"
IMAGE_NAME="hw2-$RANDOM_SIX"

echo -e "LAUNCH_TEMPLATE_NAME=\"$LAUNCH_TEMPLATE_NAME\"" >> "variables.sh" 
echo -e "ASG_NAME=\"$ASG_NAME\"" >> "variables.sh"
echo -e "AWS_ACCOUNT_ID=\"$AWS_ACCOUNT_ID\"" >> "variables.sh"
echo -e "INSTANCE_PROFILE_NAME=\"$INSTANCE_PROFILE_NAME\"" >> "variables.sh"


echo "create key pair $KEY_PEM to connect to instances and save locally"
aws ec2 create-key-pair --key-name $KEY_NAME | jq -r ".KeyMaterial" > $KEY_PEM

# secure the key pair
chmod 400 $KEY_PEM


# echo -e "KEY_NAME=\"$KEY_NAME\"\n" >> "variables.sh" 
# echo -e "KEY_PEM=\"$KEY_PEM\"\n" >> "variables.sh" 
