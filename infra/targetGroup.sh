#!/bin/bash

source securityGroup.sh

# Create target group
TARGET_GROUP_ARN=$(aws elbv2 create-target-group \
  --name "${TARGET_GROUP_NAME}" \
  --protocol HTTP \
  --port 5000 \
  --vpc-id $VPC_ID \
  --health-check-protocol HTTP \
  --health-check-port 5000 \
  --health-check-path "/health" \
  --health-check-enabled \
  --health-check-interval-seconds 30 \
  --health-check-timeout-seconds 5 \
  --healthy-threshold-count 2 \
  --unhealthy-threshold-count 2 \
  --target-type instance \
  --query 'TargetGroups[0].TargetGroupArn' \
  --output text)

echo "Target group ARN: $TARGET_GROUP_ARN"

echo -e "TARGET_GROUP_ARN=\"${TARGET_GROUP_ARN}\"" >> "variables.sh" 
