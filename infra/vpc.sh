#!/bin/bash

source defaults.sh

# Set your desired values
VPC_NAME="homework2-vpc"               # Name of your VPC

# Create VPC and retrieve its ID
VPC_ID=$(aws ec2 create-vpc \
  --cidr-block 10.0.0.0/16 \
  --query 'Vpc.VpcId' \
  --output text)

# Add a name tag to the VPC
aws ec2 create-tags \
  --resources $VPC_ID \
  --tags Key=Name,Value=$VPC_NAME

echo "VPC ID: $VPC_ID"

echo -e "VPC_ID=\"${VPC_ID}\"" >> "variables.sh" 
