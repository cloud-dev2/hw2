#!/bin/bash

source internetGateway.sh

# Create security group
SECURITY_GROUP_ID=$(aws ec2 create-security-group \
  --group-name $SECURITY_GROUP_NAME \
  --description "Security group for EC2 instances" \
  --vpc-id $VPC_ID \
  --query 'GroupId' \
  --output text)

# Configure inbound rules for security group to allow HTTP traffic on port 80
aws ec2 authorize-security-group-ingress \
  --group-id $SECURITY_GROUP_ID \
  --protocol tcp \
  --port 22 \
  --cidr 0.0.0.0/0 \
  --output text

# Configure inbound rules for security group to allow HTTP traffic on port 80
aws ec2 authorize-security-group-ingress \
  --group-id $SECURITY_GROUP_ID \
  --protocol tcp \
  --port 5000 \
  --cidr 0.0.0.0/0 \
  --output text


echo "Security group ID: $SECURITY_GROUP_ID"

echo -e "SECURITY_GROUP_ID=\"${SECURITY_GROUP_ID}\"" >> "variables.sh"
