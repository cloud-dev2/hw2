.PHONY: up down

up: 
	cd infra; bash instances.sh
	
down:
	cd infra; bash down.sh

test:
	cd infra; bash test.sh